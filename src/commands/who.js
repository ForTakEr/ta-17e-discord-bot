module.exports = async (msg) => {
  if (msg.trim() !== '*who' && msg.indexOf('SimoBot') === -1) {
    return false;
  }
  return `Hey, I'm SimoBot (${process.env.CI_COMMIT_SHORT_SHA || 'xxx'}) and Simo Tõnu Prees is my creator!
You can find my code at: https://gitlab.com/ForTakEr/ta-17e-discord-bot`;
};
