module.exports = async (msg) => {
  if (msg.trim() !== '*help' && msg.trim().toLowerCase() !== '*help!simobot') {
    return false;
  }
  return `I have the following commands available: 
   * *help / *help!SimoBot - show help about commands 
   * *who (or say my name) - share info about myself 
   * *8ball - sends a random message from a collection
   `;
};
