const help = require('./help');

describe('help', () => {
  it('random string', async () => {
    const res = await help('hdfsh sdj dk sdjsdjf');
    expect(res).toBeFalsy();
  });
  it('*help', async () => {
    const res = await help('*help');
    expect(res).toBeTruthy();
  });
  it('*help!randomname', async () => {
    const res = await help('*help!randomname');
    expect(res).toBeFalsy();
  });
  it('*help!SimoBot', async () => {
    const res = await help('*help!SimoBot');
    expect(res).toBeTruthy();
  });
});
