const who = require('./who');
const help = require('./help');
const ball = require('./8ball');

module.exports = [
  who,
  help,
  ball,
];
