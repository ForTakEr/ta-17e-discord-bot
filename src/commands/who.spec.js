const who = require('./who');

describe('who', () => {
  it('no name given in msg', async () => {
    const res = await who('hdfsh sdj dk sdjsdjf');
    expect(res).toBeFalsy();
  });
  it('*who', async () => {
    const res = await who('*who');
    expect(res).toBeTruthy();
  });
  it('bot name given in message', async () => {
    const res = await who('test SimoBot dsfjsdfj');
    expect(res).toBeTruthy();
    expect(res).toMatch(/Simo Tõnu Prees/);
  });
});
