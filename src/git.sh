#/bin/sh

# Fetch the newest code
git fetch gitlab master

# Hard reset
git reset --hard gitlab/master

# Force pull
git pull gitlab master --force